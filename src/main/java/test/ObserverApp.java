package test;

import observer.*;

public class ObserverApp {

    public static void main(String[] args) {
        Model model = new Model(50, 10);
        Observer tableView = new TableView();
        Observer pieChartView = new PieChartView();
        Observer barChartView = new BarChartView();

        model.attach(tableView);
        model.attach(barChartView);
        model.attach(pieChartView);

        model.setMaxAge(100);
        model.setMinAge(10);
    }
}
