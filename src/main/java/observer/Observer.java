package observer;

public interface Observer {
    void notifyChange(Model model);
}
